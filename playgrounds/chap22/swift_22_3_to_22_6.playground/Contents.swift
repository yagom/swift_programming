import Swift

// 코드 22-3 제네릭을 사용하지 않은 swapTwoInts(_:_:) 함수
func swapTwoInts(_ a: inout Int, _ b: inout Int) {
    let temporaryA: Int = a
    a = b
    b = temporaryA
}

var numberOne: Int = 5
var numberTwo: Int = 10

swapTwoInts(&numberOne, &numberTwo)

print("\(numberOne), \(numberTwo)") // 10, 5


// 코드 22-4 제네릭을 사용하지 않은 swapTwoStrings(_:_:) 함수
func swapTwoStrings(_ a: inout String, _ b: inout String) {
    let temporaryA: String = a
    a = b
    b = temporaryA
}

var stringOne: String = "A"
var stringTwo: String = "B"

swapTwoStrings(&stringOne, &stringTwo)

print("\(stringOne), \(stringTwo)") // "B, A"


// 코드 22-5 제네릭을 사용하지 않은 swapTwoValues(_:_:) 함수
func swapTwoValues(_ a: inout Any, _ b: inout Any) {
    let temporaryA: Any = a
    a = b
    b = temporaryA
}

var anyOne: Any = 1
var anyTwo: Any = "Two"

swapTwoValues(&anyOne, &anyTwo)

print("\(anyOne), \(anyTwo)") // "Two", 1


anyOne = stringOne
anyTwo = stringTwo

swapTwoValues(&anyOne, &anyTwo)

print("\(anyOne), \(anyTwo)") // "A, B"
print("\(stringOne), \(stringTwo)") // "B, A"

swapTwoValues(&stringOne, &stringTwo)   // 오류 – Any 외 다른 타입의 전달인자 전달 불가


// 코드 22-6 제네릭을 사용한 swapTwoValues(_:_:) 함수
func swapTwoValues<T>(_ a: inout T, _ b: inout T) {
    let temporaryA: T = a
    a = b
    b = temporaryA
}

swapTwoValues(&numberOne, &numberTwo)
print("\(numberOne), \(numberTwo)") // 10, 5

swapTwoValues(&stringOne, &stringTwo)
print("\(stringOne), \(stringTwo)") // "B, A"

swapTwoValues(&anyOne, &anyTwo)
print("\(anyOne), \(anyTwo)") // "Two", 1

swapTwoValues(&numberOne, &stringOne)   // 오류!! - 같은 타입끼리만 교환 가능

